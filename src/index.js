import express from "express";
import rootRoute from "./routes/rootRoute.js";
import cors from "cors";

const app = express();
app.use(express.json());
app.use(express.static("."));

app.use(cors());

app.listen(8080);
app.use(rootRoute);
