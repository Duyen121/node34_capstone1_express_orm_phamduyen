import express from "express";
import {
  addComment,
  getUserInfo,
  updateUserInfo,
  uploadAvatar,
  userLogin,
  userSignUp,
} from "../controllers/userController.js";
import { upload } from "../controllers/uploadController.js";
import { checkApi } from "../config/jwt.js";

const userRoute = express.Router();

userRoute.post("/sign-up", userSignUp);

userRoute.post("/login", userLogin);

userRoute.put("/update-info", checkApi, updateUserInfo);

userRoute.put("/upload-avatar", upload.single("file"), checkApi, uploadAvatar);

userRoute.post("/add-comment/:imgId", checkApi, addComment);

userRoute.get("/get-user-info", checkApi, getUserInfo);

export default userRoute;
