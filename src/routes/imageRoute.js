import express from "express";
import {
  getImgList,
  getImgByName,
  deleteImg,
  getImgByImgId,
  getCommentByImgId,
  getSavedOrUnsavedImgByImgId,
  addImg,
  getCreatedImgByUser,
  getSavedImgByUser,
} from "../controllers/imageController.js";
import { checkApi } from "../config/jwt.js";
import { upload } from "../controllers/uploadController.js";

const imageRoute = express.Router();

imageRoute.get("/get-img-by-name/:imgName", checkApi, getImgByName);
imageRoute.get("/get-img-list", checkApi, getImgList);
imageRoute.get("/get-img-detail/:imgId", checkApi, getImgByImgId);
imageRoute.get("/get-img-comment/:imgId", checkApi, getCommentByImgId);
imageRoute.get("/is-saved/:imgId", checkApi, getSavedOrUnsavedImgByImgId);
imageRoute.get("/get-created-img-by-user", checkApi, getCreatedImgByUser);
imageRoute.get("/get-saved-img-by-user", checkApi, getSavedImgByUser);

imageRoute.post("/add-img", upload.single("file"), checkApi, addImg);
imageRoute.delete("/delete-img/:imgId", checkApi, deleteImg);

export default imageRoute;
