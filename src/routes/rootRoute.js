import express from "express";
import imageRoute from "./imageRoute.js";
import userRoute from "./userRoutes.js";

const rootRoute = express.Router();

rootRoute.use("/img", imageRoute);
rootRoute.use("/user", userRoute);

export default rootRoute;
