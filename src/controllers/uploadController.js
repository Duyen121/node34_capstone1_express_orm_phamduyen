import multer, { diskStorage } from "multer";
import compress_images from "compress-images";

const upload = multer({
  storage: diskStorage({
    destination: process.cwd() + "/public/img",
    filename: (req, file, callback) =>
      callback(null, new Date().getTime() + "_" + file.originalname),
  }),
});

const optimizeImgSize = (file) => {
  if (file.size > 500000)
    compress_images(
      process.cwd() + "/public/img/" + file.filename,
      process.cwd() + "/public/file/",
      { compress_force: false, statistic: true, autoupdate: true },
      false,
      { jpg: { engine: "mozjpeg", command: ["-quality", "20"] } },
      { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
      { svg: { engine: "svgo", command: "--multipass" } },
      {
        gif: {
          engine: "gifsicle",
          command: ["--colors", "64", "--use-col=web"],
        },
      }
    );
};

export { optimizeImgSize, upload };
