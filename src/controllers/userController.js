import bcrypt from "bcrypt";
import { createToken, decodeToken } from "../config/jwt.js";
import { PrismaClient } from "@prisma/client";
import { optimizeImgSize } from "./uploadController.js";

const prisma = new PrismaClient();

const userSignUp = async (req, res) => {
  let { full_name, email, pass_word, age } = req.body;

  const checkEmail = await prisma.users.findMany({
    where: {
      email: email,
    },
  });

  if (checkEmail.length > 0) {
    res.send("Email exists");
    return;
  }
  let newPass = bcrypt.hashSync(pass_word, 10);

  let newUser = {
    full_name,
    email,
    pass_word: newPass,
    avatar: "",
    age,
  };

  await prisma.users.create({ data: newUser });

  res.send("Signed up sucessfully");
};

const userLogin = async (req, res) => {
  let { email, pass_word } = req.body;

  let checkEmail = await prisma.users.findFirst({
    where: {
      email: email,
    },
  });

  if (checkEmail) {
    let checkPass = bcrypt.compareSync(pass_word, checkEmail.pass_word);
    if (checkPass) {
      let token = createToken({ checkEmail });
      res.send(token);
    } else {
      res.send("Incorrect password");
    }
  } else {
    res.send("Incorrect email");
  }
};

const updateUserInfo = async (req, res) => {
  let { full_name, email, pass_word, age } = req.body;
  let { token } = req.headers;
  let user = decodeToken(token);
  let { id } = user.data.checkEmail;
  let userInfo = await prisma.users.findUnique({
    where: {
      id,
    },
  });
  let newPass = bcrypt.hashSync(pass_word, 10);

  let updateInfo = { ...userInfo, full_name, email, pass_word: newPass, age };
  await prisma.users.update({
    where: {
      id,
    },
    data: updateInfo,
  });
  res.send("Updated user info successfully");
};

const uploadAvatar = async (req, res) => {
  let file = req.file;
  let img = await optimizeImgSize(file);
  let { token } = req.headers;
  let user = decodeToken(token);
  let { id } = user.data.checkEmail;
  let userInfo = await prisma.users.findUnique({
    where: {
      id,
    },
  });
  let updateAvatar = { ...userInfo, avatar: file.filename };
  await prisma.users.update({
    where: {
      id,
    },
    data: updateAvatar,
  });
  res.send("Updated user avatar successfully");
};

const addComment = async (req, res) => {
  let { token } = req.headers;
  let user = decodeToken(token);
  let { id } = user.data.checkEmail;
  let { imgId } = req.params;
  let { content } = req.body;
  let checkUser = await prisma.users.findUnique({
    where: {
      id,
    },
  });
  let checkImg = await prisma.images.findUnique({
    where: {
      id: Number(imgId),
    },
  });

  if (checkUser && checkImg) {
    let newComment = {
      user_id: id,
      image_id: Number(imgId),
      content: content,
    };

    await prisma.image_comments.create({
      data: newComment,
    });
    res.send("Add comment successfully");
  } else {
    res.status(422).send("No user or image found");
  }
};

const getUserInfo = async (req, res) => {
  let { token } = req.headers;
  let user = decodeToken(token);
  let { id } = user.data.checkEmail;
  let userInfo = await prisma.users.findUnique({
    where: { id },
  });
  res.send(userInfo);
};

export {
  userSignUp,
  userLogin,
  updateUserInfo,
  uploadAvatar,
  getUserInfo,
  addComment,
};
