import { PrismaClient } from "@prisma/client";
import { optimizeImgSize } from "./uploadController.js";
import { decodeToken } from "../config/jwt.js";

const prisma = new PrismaClient();

const getImgByName = async (req, res) => {
  let { imgName } = req.params;
  let data = await prisma.images.findMany({
    where: {
      name: {
        contains: imgName,
      },
    },
  });
  if (data.length > 0) {
    res.status(200).send(data);
  } else {
    res.send("No result");
  }
};

const getImgList = async (req, res) => {
  let data = await prisma.images.findMany({
    include: {
      image_comments: true,
    },
  });
  res.send(data);
};

const getImgByImgId = async (req, res) => {
  let { imgId } = req.params;
  let data = await prisma.images.findFirst({
    where: {
      id: Number(imgId),
    },
  });
  if (data) {
    res.send(data);
  } else {
    res.status(422).send("Image doesn't exist");
  }
};

const getSavedOrUnsavedImgByImgId = async (req, res) => {
  let { imgId } = req.params;
  let data = await prisma.saved_images.findMany({
    where: {
      id: Number(imgId),
    },
  });
  if (data.length > 0) {
    res.send("Image is saved");
  } else {
    res.send("Image is not saved");
  }
};

const getSavedImgByUser = async (req, res) => {
  let { token } = req.headers;
  let user = decodeToken(token);
  let { id } = user.data.checkEmail;
  let data = await prisma.saved_images.findMany({
    where: {
      user_id: id,
    },
  });
  if (data.length > 0) {
    res.send(data);
  } else {
    res.send("No result");
  }
};

const getCreatedImgByUser = async (req, res) => {
  let { token } = req.headers;
  let user = decodeToken(token);
  let { id } = user.data.checkEmail;
  let data = await prisma.images.findMany({
    where: {
      user_id: id,
    },
  });
  res.send(data);
};

const getCommentByImgId = async (req, res) => {
  let { imgId } = req.params;
  let data = await prisma.image_comments.findMany({
    where: {
      image_id: Number(imgId),
    },
  });
  if (data.length > 0) {
    res.send(data);
  } else {
    res.send("No result");
  }
};

//
const addImg = async (req, res) => {
  let file = req.file;
  let { token } = req.headers;
  let { name, desc } = req.body;

  let img = await optimizeImgSize(file);
  let user = decodeToken(token);
  let { id } = user.data.checkEmail;
  let checkUser = await prisma.users.findUnique({
    where: {
      id,
    },
  });
  if (checkUser) {
    let newImg = {
      name: name,
      image_link: file.filename,
      desc: desc,
      user_id: id,
    };
    await prisma.images.create({
      data: newImg,
    });
    res.send("Create new image successfully");
  } else {
    res.status(422).send("User doesn't exist");
  }
};

const deleteImg = async (req, res) => {
  let { imgId } = req.params;
  let img = await prisma.images.findUnique({
    where: {
      id: Number(imgId),
    },
  });
  if (img) {
    await prisma.images.delete({
      where: {
        id: Number(imgId),
      },
    });
    res.send("Delete image successfully");
  } else {
    res.status(422).send("Image doesn't exist");
  }
};

export {
  getImgByName,
  getImgList,
  addImg,
  getCreatedImgByUser,
  getImgByImgId,
  getCommentByImgId,
  getSavedOrUnsavedImgByImgId,
  getSavedImgByUser,
  deleteImg,
};
