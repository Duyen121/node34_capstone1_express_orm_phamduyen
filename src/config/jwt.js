import jwt from "jsonwebtoken";

const createToken = (data) => {
  let token = jwt.sign({ data }, "TOKEN", {
    algorithm: "HS256",
    expiresIn: "5y",
  });

  return token;
};

const checkToken = (token) => {
  return jwt.verify(token, "TOKEN");
};

const decodeToken = (token) => {
  return jwt.decode(token);
};

const checkApi = (req, res, next) => {
  try {
    let { token } = req.headers;
    checkToken(token);
    next();
  } catch (exception) {
    res.status(401).send("Unauthorized");
  }
};

export { createToken, checkToken, decodeToken, checkApi };
